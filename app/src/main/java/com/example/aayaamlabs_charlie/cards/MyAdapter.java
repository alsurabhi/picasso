package com.example.aayaamlabs_charlie.cards;

/**
 * Created by aayaamlabs-charlie on 18/3/17.
 */
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<Createlist> galleryList;
    private Context context;

    public MyAdapter(Context context, ArrayList<Createlist> galleryList) {
        this.galleryList = galleryList;
        this.context = context;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.simple_image, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyAdapter.ViewHolder viewHolder, final int i) {

        viewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context).load(galleryList.get(i).getImage_id()).resize(200,200).into(viewHolder.img);

    }


    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        private Button b1;
        private ImageView s1;
        public ViewHolder(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.img);
            b1 = (Button)view.findViewById(R.id.button1);
            s1 = (ImageView)view.findViewById(R.id.share);

            b1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "Removing Card", Toast.LENGTH_SHORT).show();
                 //    String j = galleryList.get(getLayoutPosition()).getImage_id();
//                    Log.i("sdsd",j);
//                    DatabaseHandler db = new DatabaseHandler(context);
//                    db.removelabel(j);
//                    delete(getLayoutPosition());
//                    notifyItemRemoved(getLayoutPosition());
//                    notifyItemRangeChanged(getLayoutPosition(), galleryList.size());
                    final AlertDialog.Builder alertbox = new AlertDialog.Builder(view.getRootView().getContext());
                    alertbox.setMessage("Are You Sure");
                    alertbox.setTitle("Warning");

                    alertbox.setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0,
                                                    int arg1) {
                                        String j = galleryList.get(getLayoutPosition()).getImage_id();
                    Log.i("sdsd",j);
                    DatabaseHandler db = new DatabaseHandler(context);
                    db.removelabel(j);
                    delete(getLayoutPosition());
                                }
                            });
                    alertbox.setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0,
                                                    int arg1){
                                    arg0.dismiss();
                                }

                    });
                    alertbox.show();

                }
            });
            s1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    shareIt();
                }
            });

        }


        private void shareIt() {
//sharing implementation here
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = "Here is the share content body";
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

        }

        public void delete(int position) { //removes the row
            galleryList.remove(position);
            notifyItemRemoved(position);
        }
    }

}
