package com.example.aayaamlabs_charlie.cards;

/**
 * Created by aayaamlabs-charlie on 24/2/17.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daprlabs.cardstack.SwipeDeck;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;



public class SwipeDeckActivity extends AppCompatActivity {

    public static final String ROOT_URL = "http://45.118.133.182:8080/index.php/wp-json/wp/v2/";
    private static final String TAG = "MainActivity";
    private SwipeDeck cardStack;
    private Context context = this;

    private List<Book> books;


    private SwipeDeckAdapter adapter;
    public ArrayList<String> testData;
    public ArrayList<String> allqtys;
    public static String[] eatFoodyImages = {
            "http://i.imgur.com/rFLNqWI.jpg",
            "http://i.imgur.com/C9pBVt7.jpg",
            "http://i.imgur.com/rT5vXE1.jpg",
            "http://i.imgur.com/aIy5R2k.jpg",
            "http://i.imgur.com/MoJs9pT.jpg",
            "http://i.imgur.com/S963yEM.jpg",
            "http://i.imgur.com/rLR2cyc.jpg",
            "http://i.imgur.com/SEPdUIx.jpg",
            "http://i.imgur.com/aC9OjaM.jpg",
            "http://i.imgur.com/76Jfv9b.jpg",
            "http://i.imgur.com/fUX7EIB.jpg",
            "http://i.imgur.com/syELajx.jpg",
            "http://i.imgur.com/COzBnru.jpg",
            "http://i.imgur.com/Z3QjilA.jpg",
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBooks();

    }

    private void getBooks(){

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        BooksAPI api = adapter.create(BooksAPI.class);

        //Defining the method
        api.getBooks(new Callback<List<Book>>() {
            @Override
            public void success(List<Book> list, Response response) {


                //Storing the data in our list
                books = list;
                //Calling a method to show the list
                showList();
            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
            }
        });
    }
    private void showList(){
        //String array to store all the book names
        String[] items = new String[books.size()];
        allqtys=new ArrayList<String>();
        //Traversing through the whole list to get all the names
        for(int i=0; i<books.size(); i++){
            //Storing names to string array
            items[i] = books.get(i).getSlug();
            //Toast.makeText(context, items[i], Toast.LENGTH_SHORT).show();
            allqtys.add(items[i]);
        }

        testData = (ArrayList)allqtys.clone();
        System.out.println("All MinQuantitiy"+testData.size());
//        //Creating an array adapter for list view
//        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.simple_list,items);
//
//        //Setting adapter to listview
//        listView.setAdapter(adapter);
        setContentView(R.layout.activity_swipe_deck);

        cardStack = (SwipeDeck) findViewById(R.id.swipe_deck);
        cardStack.setHardwareAccelerationEnabled(true);
//        testData = new ArrayList<>();
        System.out.println("All MinQuantitiy"+testData.size());
//        testData.add("0");
//        testData.add("1");
//        testData.add("2");
//        testData.add("3");
//        testData.add("4");
        //Traversing through the whole list to get all the names
//        for(int i=0; i<10; i++){
//
//
//            //Storing names to string array
//            testData.add(String.valueOf(i));
//
//
//        }

        adapter = new SwipeDeckAdapter(testData, this);
        cardStack.setAdapter(adapter);

        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                Log.i("MainActivity", "card was swiped left, position in adapter: " + position);
                Toast.makeText(context,"You Disliked the Card", Toast.LENGTH_SHORT).show();
//                Intent myIntent = new Intent(SwipeDeckActivity.this,ShowDetail.class);
//                startActivity(myIntent);

            }

            @Override
            public void cardSwipedRight(int position) {
                Log.i("MainActivity", "card was swiped right, position in adapter: " + position);
                Toast.makeText(context,"You Liked the Card", Toast.LENGTH_SHORT).show();
                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                db.insertLabel(eatFoodyImages[position]);

            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
            }

            @Override
            public void cardActionDown() {
                Log.i(TAG, "cardActionDown");
            }

            @Override
            public void cardActionUp() {
                Log.i(TAG, "cardActionUp");
            }

        });
        cardStack.setLeftImage(R.id.left_image);
        cardStack.setRightImage(R.id.right_image);

//        Button btn = (Button) findViewById(R.id.button);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cardStack.swipeTopCardLeft(180);
//
//            }
//        });
        Button btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // testData.add("a sample string.");
                cardStack.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });

        Button btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadSpinnerData();
              //  cardStack.swipeTopCardRight(180);
//                ArrayList<String> newData = new ArrayList<>();
//                newData.add("some new data");
//                newData.add("some new data");
//                newData.add("some new data");
//                newData.add("some new data");
//
//                SwipeDeckAdapter adapter = new SwipeDeckAdapter(newData, context);
//                cardStack.setAdapter(adapter);

            }
        });


    }



    private void loadSpinnerData() {
        DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        ArrayList<String> labels = db.getAllLabels();
       // Log.i("MiainActivity",labels.toString());
       Intent myintent = new Intent(SwipeDeckActivity.this,ShowDetail.class);
       myintent.putExtra("array",labels);
        startActivity(myintent);

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_swipe_deck, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public class SwipeDeckAdapter extends BaseAdapter {

        private List<String> data;
        private Context context;

        public SwipeDeckAdapter(List<String> data, Context context) {
            this.data = data;
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;
            if (v == null) {
                LayoutInflater inflater = getLayoutInflater();
                // normally use a viewholder
                v = inflater.inflate(R.layout.test_card2, parent, false);
            }
            //((TextView) v.findViewById(R.id.textView2)).setText(data.get(position));
            ImageView imageView = (ImageView) v.findViewById(R.id.offer_image);
            Picasso.with(context).load(eatFoodyImages[position]).fit().into(imageView);
            TextView textView = (TextView) v.findViewById(R.id.sample_text);
            String item = (String)getItem(position);
            textView.setText(item);
            //Log.i("surabhi",eatFoodyImages[position]);
           final String pos = String.valueOf(position);
            //Log.i("number",pos);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Layer type: ", Integer.toString(v.getLayerType()));
                    Log.i("Hwardware Accel type:", Integer.toString(View.LAYER_TYPE_HARDWARE));
                    Intent i = new Intent(v.getContext(), BlankActivity.class);
                    i.putExtra("name",pos);
                    v.getContext().startActivity(i);
                    //Log.i("clicked",pos);
                }
            });
            return v;
        }
    }
}