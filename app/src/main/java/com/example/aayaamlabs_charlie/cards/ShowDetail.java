package com.example.aayaamlabs_charlie.cards;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import java.util.ArrayList;


/**
 * Created by aayaamlabs-charlie on 8/3/17.
 */

public class ShowDetail extends AppCompatActivity {



  //private Swipe adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.list);

        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(),1);
        recyclerView.setLayoutManager(layoutManager);
        Intent i = getIntent();
       ArrayList stock_list = i.getStringArrayListExtra("array");
        Object[] arrayOfObjects = stock_list.toArray();
         String[] items = new String[arrayOfObjects.length];
         String[] lv_arr  = new String[arrayOfObjects.length];
        int l = arrayOfObjects.length;
        for (int j=0; j<arrayOfObjects.length; j++) {

            items[j] = arrayOfObjects[j].toString();
            lv_arr[j] = items[j];
           // Log.v("MyTag", Integer.toString(j) + ":" + lv_arr[j]);
        }
        String len = String.valueOf(l);
        ArrayList<Createlist> createLists = prepareData(len,lv_arr);
        MyAdapter adapter = new MyAdapter(getApplicationContext(), createLists);


        recyclerView.setAdapter(adapter);
    }
    private ArrayList<Createlist> prepareData(String length,String z[]){
       int p = Integer.parseInt(length);
        ArrayList<Createlist> theimage = new ArrayList<>();
        for(int i = 0; i< p; i++){
            Createlist createList = new Createlist();
            createList.setImage_id(z[i]);

            theimage.add(createList);
        }
        return theimage;
    }
    }
