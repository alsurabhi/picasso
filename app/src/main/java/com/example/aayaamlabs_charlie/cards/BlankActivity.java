package com.example.aayaamlabs_charlie.cards;

/**
 * Created by aayaamlabs-charlie on 24/2/17.
 */

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class BlankActivity extends AppCompatActivity {

    public static final String ROOT_URL = "http://45.118.133.182:8080/index.php/wp-json/wp/v2/";
    private List<Book> books;

    TextView id;
    TextView slug;
    TextView type;
    TextView date;
    TextView status;
    TextView author;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBooks();

    }
    private void getBooks(){

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        BooksAPI api = adapter.create(BooksAPI.class);

        //Defining the method
        api.getBooks(new Callback<List<Book>>() {
            @Override
            public void success(List<Book> list, Response response) {


                //Storing the data in our list
                books = list;
                //Calling a method to show the list
                showList();
            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
            }
        });
    }
    private void showList(){
        //String array to store all the book names
        String[] items = new String[books.size()];
        String[] s = new String[books.size()];
        String[] t = new String[books.size()];
        String[] d = new String[books.size()];
        String[] st = new String[books.size()];
        String[] a = new String[books.size()];

        //Traversing through the whole list to get all the names
        for(int i=0; i<books.size(); i++){
            //Storing names to string array
            items[i] = books.get(i).getId();
            s[i] = books.get(i).getSlug();
            t[i] = books.get(i).getType();
            d[i] = books.get(i).getDate();
            st[i] = books.get(i).getStatus();
            a[i] = books.get(i).getAuthor();

            Log.v("MyTag", Integer.toString(i) + ":" + items[i]);

        }

        String position = getIntent().getExtras().getString("name");
        int pos = Integer.parseInt(position);
        setContentView(R.layout.activity_blank);

        id = (TextView)findViewById(R.id.IDRdate);
        slug = (TextView)findViewById(R.id.IDdate);
        type = (TextView)findViewById(R.id.Idate);
        date = (TextView)findViewById(R.id.date);
        status = (TextView)findViewById(R.id.status);
        author = (TextView)findViewById(R.id.author);


        id.setText("Id : " + items[pos]);
        slug.setText("Name : " + s[pos]);
        type.setText("Format : " +t[pos]);
        date.setText("Date : " +d[pos]);
        status.setText("Status : " +st[pos]);
        author.setText("Author : " +a[pos]);



    }

}