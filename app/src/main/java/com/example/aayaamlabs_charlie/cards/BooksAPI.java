package com.example.aayaamlabs_charlie.cards;

/**
 * Created by aayaamlabs-charlie on 23/2/17.
 */

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Belal on 11/3/2015.
 */
public interface BooksAPI {

    /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */
    @GET("/posts")
    public void getBooks(Callback<List<Book>> response);
}
